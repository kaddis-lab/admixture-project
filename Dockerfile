FROM ubuntu:18.04
MAINTAINER Anh Nguyet Vu <avu@coh.org>

WORKDIR $HOME/project

ENV DEBIAN_FRONTEND=noninteractive

# Add pipeline dependencies:
# - R
# - PLINK (both 1.9 and 2-alpha)
# - ADMIXTURE 1.3.0 (https://dalexander.github.io/admixture/download.html)

RUN apt-get update && apt-get install -y --no-install-recommends \
# some utils
    wget \
    bc \
    locales \
# R and dependencies
    r-base \
    build-essential  

# Install, PLINK1.9, PLINK2-alpha, ADMIXTURE
RUN wget http://s3.amazonaws.com/plink1-assets/plink_linux_x86_64_20201019.zip && \
    unzip plink_linux_x86_64_20201019.zip plink && \
    rm plink_linux_x86_64_20201019.zip && \
    mv plink /usr/local/bin && \
# PLINK2
    wget http://s3.amazonaws.com/plink2-assets/plink2_linux_x86_64_20210302.zip && \
    unzip plink2_linux_x86_64_20210302.zip plink2 && \
    rm plink2_linux_x86_64_20210302.zip && \
    mv plink2 /usr/local/bin && \
# ADMIXTURE1.3
    wget https://dalexander.github.io/admixture/binaries/admixture_linux-1.3.0.tar.gz && \
    tar -zxf admixture_linux-1.3.0.tar.gz dist/admixture_linux-1.3.0/admixture && \
    rm admixture_linux-1.3.0.tar.gz && \
    mv dist/admixture_linux-1.3.0/admixture /usr/local/bin && \
    rm -R dist

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US  
ENV LC_COLLATE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

COPY plot.R .
    
