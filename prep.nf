#!/usr/bin/env nextflow

/*
 * Parameters
 */

// params.ref = "1KGenomes" // this param will be relevant when pipeline allows setting up different reference cohorts
params.dataDir = "$projectDir/data/all_phase3"
params.memory = 12000 // min requirement is 12 GiB RAM
params.threads = 12

/*
 * Download, extract reference and create accessory files
 * https://www.cog-genomics.org/plink/2.0/resources#1kg_phase3
*/
process download_setup_reference {

   publishDir "${params.dataDir}", mode: "move"

   output:
   file("*.{bed,bim,fam}") into ref_plink
   file("all_phase3_snps") into ref_snps
   file("all_phase3.pop") into ref_pop

   shell:
   '''
   wget -O all_phase3.pgen.zst https://www.dropbox.com/s/afvvf1e15gqzsqo/all_phase3.pgen.zst?dl=1
   wget -O all_phase3.pvar.zst https://www.dropbox.com/s/op9osq6luy3pjg8/all_phase3.pvar.zst?dl=1
   wget -O all_phase3.psam https://www.dropbox.com/s/yozrzsdrwqej63q/phase3_corrected.psam?dl=1
   plink2 --zst-decompress all_phase3.pgen.zst > all_phase3.pgen
   plink2 --memory !{params.memory} --threads !{params.threads} --pfile all_phase3 vzs --max-alleles 2 --make-bed --out all_phase3
   plink2 --memory !{params.memory} --threads !{params.threads} --bfile all_phase3 --king-cutoff 0.177 --allow-extra-chr --snps-only --make-bed --out all_phase3
   awk '{print $2}' all_phase3.bim | sort | uniq -u > all_phase3_snps
   awk -F'\t' 'NR==FNR{c[$2]++;next};c[$1]>0 {print $5}' all_phase3.king.cutoff.in.id all_phase3.psam > all_phase3.pop
   '''
}


