## Ancestry and Ancestry-based Genetic Risk Score Analysis

This is the `nextflow` pipeline for (1) supervised ADMIXTURE (v1.3)[^admixture] **ancestry analysis** and (2) (soon) computation of different ancestry-based/non-ancestry-based genetic risk scores (GRS) for T1D. Potentially this can be a one-stop shop to **benchmark and compare previous published and newly developed GRS methods for individuals/a patient cohort**. Current development is for nPOD cohort data from the University of Florida, 23andMe data, and sufficiently similar SNPchip data. Part (1) of the pipeline is ready and can be run locally as well as on our cloud app (see doc site). We are still working on part (2) with collaborators.

## Documentation
See our official documentation site for usage and news: https://kaddis-lab.gitlab.io/admixture-project/

## Contact
Pipeline author/maintainer: Anh Nguyet Vu <avu@coh.org>

### References
[^admixture]: D.H. Alexander, J. Novembre, and K. Lange. Fast model-based estimation of ancestry in unrelated individuals. Genome Research, 19:1655–1664, 2009. 
