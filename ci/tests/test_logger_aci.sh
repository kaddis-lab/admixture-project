#!/bin/bash

echo "set USER_UUID as $USER_UUID"
echo "set PROJECT_UUID as $PROJECT_UUID"
echo "testing $DART_ENDPOINT"
cd docker-app/aci/
set -m
Rscript entrypoint.R & nextflow -q run hello
pkill R
grep "status code 200" data/userdata/$USER_UUID/$PROJECT_UUID/log/*.txt
