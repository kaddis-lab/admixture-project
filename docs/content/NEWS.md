---
date: 2020-12-24
title: "NEWS"
---

## 0.5.0 (current development)
**Expanded data and logging**
New features:
- Add support for 23andMe data in original .txt format [#22](https://gitlab.com/kaddis-lab/admixture-project/-/issues/22)
- Add `.png` output in addition to `.pdf` [#46](https://gitlab.com/kaddis-lab/admixture-project/-/issues/46)

## 0.5.0 - cloud version
- Change to log digest (simpler metadata logs) for custom logger [#45](https://gitlab.com/kaddis-lab/admixture-project/-/issues/22)
- Output addition debug files in addition to sending to endpoint

## 0.4.0 
**More standard base version for nextflow users, binary updates and bug fixes**
- Refactor Docker image base to be more for nextflow standard runs and less for cloud
- Changes to pipeline and config for new intended default run using `--with-docker`
- Update PLINK2-alpha version [#39](https://gitlab.com/kaddis-lab/admixture-project/-/issues/39)
- Fix bug in `prep.nf` [#40](https://gitlab.com/kaddis-lab/admixture-project/-/issues/40)

## 0.4.0 - cloud version
- Support for passing in custom log endpoint through new ENV

## 0.3.0
**Helpers for using pipeline.**
- Add `prep.nf` pipeline to help set up files before main run [#16](https://gitlab.com/kaddis-lab/admixture-project/-/issues/16)
- Add user guide/doc site [#20](https://gitlab.com/kaddis-lab/admixture-project/-/issues/20)

## 0.3.0 - cloud version
**Cloud version of containerized pipeline adds additional configuration and programming to interface with web app.**
- Handle and report custom JSON metadata to endpoint for task/job processes [#27](https://gitlab.com/kaddis-lab/admixture-project/-/issues/27)


## 0.2.0
**Improve automation, robustness and correctness for handling other data by implementing programmatic checks for manual interactive checks and applying more filters. Also new features and improved portability.**

- Additional filter for long-range LD [#8](https://gitlab.com/kaddis-lab/admixture-project/-/issues/8)
- Additional filter for relatedness [#15](https://gitlab.com/kaddis-lab/admixture-project/-/issues/15)
- Programmatic check for compatible variant IDs [#14](https://gitlab.com/kaddis-lab/admixture-project/-/issues/14)
- Programmatic check for build [#4](https://gitlab.com/kaddis-lab/admixture-project/-/issues/4)
- Programmatic check for merge issues [#5](https://gitlab.com/kaddis-lab/admixture-project/-/issues/5)
- Programmatic post-prune check for meeting variants remaining threshold [#17](https://gitlab.com/kaddis-lab/admixture-project/-/issues/17)
- Docker image (portability) for this version [#6](https://gitlab.com/kaddis-lab/admixture-project/-/issues/6)

New features:
- Now also outputs a PDF figure from R [#2](https://gitlab.com/kaddis-lab/admixture-project/-/issues/2), [#3](https://gitlab.com/kaddis-lab/admixture-project/-/issues/3)
- Creates subsetted .csv for user data, a somewhat friendlier output than just the Q file and useful for re-plotting


## 0.1.0
**Initial conversion of interactive scripts into a pipeline using the nextflow framework, with more focus on optimization.**

- Re-implementation in nextflow (#0)
- Add new explicit/configurable parameters for some previously hard-coded ones, e.g. memory, threads, data directories (#0)
- For other previously hard-coded parameters, add code to generate based on inputs, e.g. K (#0)
- Improved intersection step to reduce resource usage/better handle potentially larger inputs [#1](https://gitlab.com/kaddis-lab/admixture-project/-/issues/1)
- Factoring out steps that don't need to be repeated in each run [#7](https://gitlab.com/kaddis-lab/admixture-project/-/issues/7)



