---
title: "Output files summary"
date: 2020-12-23
weight: 30
---

This provides more details of the output files to help users with their interpretation and usage. The plots are generated with default cutoff values, so in some cases it might be desirable to redo the plot using your own cutoff values.

{{< fileTree >}}
* user
    * nextflow.config
    * yourdata.bed
    * yourdata.bim
    * yourdata.fam
    * results
        * fig.pdf
        * merged_pruned_seed*.P
        * merged_pruned_seed*.Q
        * proportions.csv
{{< /fileTree >}} 

- `fig.pdf` is a plot of ancestry proportions for your data samples. Each bar represents an individual and their estimated proportion as color-coded by the legend at the bottom. The individuals are binned into a population group if they reach a 0.6 majority proportion for a group; otherwise, they are grouped with "Mixed". This threshold is merely a rule of thumb and changing it will change the number of individuals in the Mixed section. 
- `merged_pruned_seed*.P` is a direct output matrix from ADMIXTURE and contains variant allele frequencies for each population for both reference and your data (the last _n_ rows of the matrix).
- `merged_pruned_seed*.Q` is a direct output matrix from ADMIXTURE and contains the estimated proportions for both reference and your data (the last _n_ rows of the matrix). It is usually more convenient to use `proportions.csv`.
- `proportions.csv` is a table subsetted from the `.Q` output with labels added, containing only your data samples. It is used for generating `fig.pdf` and can be used for redoing figures. 
