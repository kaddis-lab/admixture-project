---
title: "Containerized pipeline with Docker (DEPRECATED)"
date: 2020-12-23
toc: true
weight: 100
---

{{< warning >}}
{{< /warning >}}
**This has been deprecated and will be removed soon. Redirect to updated usage [here]({{< ref "/nextflow-run-with-docker" >}}).**


## Intro 

**For those who want something even simpler, see the [web app]({{< ref "/web-app" >}}).**

This guides you through running the containerized pipeline, which works cross-platform and saves you from having to install dependencies and simplifies things into at most several steps. This route is for users who: 
1. Are somewhat familiar with Docker or are willing to learn basic interactions with Docker.
2. Strongly prefer keeping their data on-premise/doing analyses locally.
3. Want more control over some options not exposed with web app.
 

## Install Docker Engine

If you don't already have Docker Engine, follow the documentation for your platform here: https://docs.docker.com/engine/install/

## Pull image

After making sure that Docker Engine is working, pull our image with the following command:
{{< cmd >}}
docker pull registry.gitlab.com/kaddis-lab/admixture-project/dev
{{</ cmd >}}

## Setting up working directory and data

### Project working directory

Create an empty working directory. For example:
{{< cmd >}}
mkdir myproject
cd myproject
{{</ cmd >}}

Within `myproject` you will need to set up data required for the pipeline, as described below. 

### Reference data & accessory files

The pipeline requires reference data, normally 1000 Genomes data. **This reference data is not bundled in the image, but a prep pipeline is provided and easily run one-time to furnish your project with the necessary reference data and accessory files.** Most of the work here is downloading the reference data and unpacking it. 

{{< note >}}
It is possible that you already have the 1K Genomes data on-hand, i.e. from another project. If so, it makes more sense to follow the alternative preparation below to avoid a long redundant download.
{{< /note >}}

#### Provisioning through default prep pipeline

In `myproject`, run the command below. The prep pipeline will create the data and persist it on your machine for later use with the main pipeline.

{{< warning >}}
Because of the 1K Genome reference data size, make sure you have at least 12 GiB free memory to work with.
{{< /warning >}}
{{< cmd >}}
docker run -it -v $(pwd):/project/data registry.gitlab.com/kaddis-lab/admixture-project/dev:latest prep.nf
{{</ cmd >}}

Seeing something like the below will confirm that it was sucessful.

<pre>
N E X T F L O W  ~  version 20.10.0
Launching `prep.nf` [backstabbing_fermat] - revision: 7ea7eaa7de
executor >  local (2)
[e1/661f87] process > download_setup_reference [100%] 1 of 1 ✔
[0a/233d56] process > setup_default_config (1) [100%] 1 of 1 ✔

Completed at: 22-Dec-2020 21:45:58
Duration    : 38m 41s
CPU hours   : 0.6
Succeeded   : 2
</pre>

Your working directory should now be furnished with all the needed files _except your data_.

{{< figure src="prep_output.png" title="furnished working directory after prep.nf" >}}

The files are organized as diagrammed below.

{{< fileTree >}}
* myproject
    * all_phase3
        * all_phase3.bed
        * all_phase3.bim
        * all_phase3.fam
        * all_phase3_snps
        * all_phase3.pop
    * user
        * nextflow.config
{{< /fileTree >}}

#### Alternative

TO-DO

### Your data

In the created `user` directory you will notice only a `nextflow.config` file. Simply move your Plink `.bed` `.bim` `.fam` files into here alongside the default config file.

{{< fileTree >}}
  * user
      * nextflow.config
      * yourdata.bed
      * yourdata.bim
      * yourdata.fam
{{< /fileTree >}}
 
## Run

The default `nextflow.config` file does not have to be modified in most cases. See the [configuration]({{< ref "/configuration" >}}) page if you have issues with the run or want a setup different from the default. **But for a basic run, at the root of `myproject` simply use the command:**

{{< cmd >}}
docker run -it -v $(pwd):/project/data registry.gitlab.com/kaddis-lab/admixture-project/dev
{{</ cmd >}}

With a successful run, you should see the pipeline status similar to below.

<pre>
N E X T F L O W  ~  version 20.10.0
Launching `main.nf` [pedantic_bernard] - revision: 7142d45a5d
executor >  local (10)
[f0/23b7a0] process > basic_qc_filters (1)            [100%] 1 of 1 ✔
[96/b9f9d7] process > get_intersection_with_check (1) [100%] 1 of 1 ✔
[9b/62b587] process > subset_userdata (1)             [100%] 1 of 1 ✔
[74/e5df2c] process > subset_refdata (1)              [100%] 1 of 1 ✔
[2a/ec6918] process > check_build (1)                 [100%] 1 of 1 ✔
[0a/c7323e] process > merge (1)                       [100%] 1 of 1 ✔
[2d/0f0acd] process > make_pop_labels (1)             [100%] 1 of 1 ✔
[4b/bf7347] process > prune_and_check (1)             [100%] 1 of 1 ✔
[d4/f9ae9a] process > run_ADMIXTURE (1)               [100%] 1 of 1 ✔
[d5/b9acee] process > make_fig_csv (1)                [100%] 1 of 1 ✔

Completed at: 22-Dec-2020 17:45:52
Duration    : 32m 19s
CPU hours   : 0.5
Succeeded   : 10
</pre>

## View results

In the `user` directory on your host machine, you should see a `results` folder with content after the run. 

{{< fileTree >}}
* user
    * nextflow.config
    * yourdata.bed
    * yourdata.bim
    * yourdata.fam
    * results
        * fig.pdf
        * merged_pruned_seed*.P
        * merged_pruned_seed*.Q
        * proportions.csv
{{< /fileTree >}} 

For a description of these result files, refer to the [output files summary]({{< ref "/output-files-summary" >}}).

## Clean-up

In advanced cases or for troubleshooting, you can restart the stopped container to also extract intermediate files and logs. If this is not necessary, remove the stopped container.

{{< cmd >}}
docker rm ID_or_Name
{{</ cmd >}}
