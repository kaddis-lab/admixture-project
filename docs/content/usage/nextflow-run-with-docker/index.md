---
title: "Nextflow run with docker"
date: 2020-02-02
toc: true
weight: 1
---

## Intro 

**This is an alternative to the [web app]({{< ref "/web-app" >}}).**

This is the standard documentation for running the nextflow pipeline with docker (locally, on Linux). These instructions are for users who: 
- Already have and use nextflow regularly.
- Do already use Docker images for a simpler and more reproduceable workflow.
- Strongly prefer keeping their data on-premise/doing analyses locally.
- Want more control over some options not exposed with the web app.

## Check your nextflow version

The pipeline was developed with `nextflow v20.10.0`. 

## Pull image

Assuming you have Docker installed, pull our image with the following command:
```bash
docker pull registry.gitlab.com/kaddis-lab/admixture-project/base
```

## Get nextflow project

```bash
nextflow clone -hub gitlab kaddis-lab/admixture-project project
```
You can take a look at the _test_ profile in `nextflow.config` and peruse the `/test` directory skeleton to get an example of the configuration.
 

## Set up data

### Provisioning reference data & accessory files through default prep pipeline

You will still need to populate the reference data; the pipeline uses 1KGenomes data. **A prep pipeline (`prep.nf`) is provided and easily run one-time to furnish your project with the necessary reference data and accessory files.** Most of the work here is downloading the reference data and unpacking it. 

{{< warning >}}
Because of the 1KGenomes reference data size, make sure you have at least 12 GiB free memory to work with.
{{< /warning >}}

Running the below will create an `all_phase3` folder in `project/data` (this might take some time).

```bash
nextflow run prep.nf
```

### Your data

Put your folder containing your plink files into `project/data`. Modify the _standard_ profile or create a new profile in `nextflow.config` to reflect what your folder is actually named and any other customized parameters.
 
## Run

Assuming you modified the _standard_ profile for the run (otherwise replace with your profile):

```bash
nextflow run main.nf -profile standard
```

With a successful run, you should see pipeline output similar to below.

<pre>
N E X T F L O W  ~  version 20.10.0
Launching `main.nf` [pedantic_bernard] - revision: 7142d45a5d
executor >  local (10)
[f0/23b7a0] process > basic_qc_filters (1)            [100%] 1 of 1 ✔
[96/b9f9d7] process > get_intersection_with_check (1) [100%] 1 of 1 ✔
[9b/62b587] process > subset_userdata (1)             [100%] 1 of 1 ✔
[74/e5df2c] process > subset_refdata (1)              [100%] 1 of 1 ✔
[2a/ec6918] process > check_build (1)                 [100%] 1 of 1 ✔
[0a/c7323e] process > merge (1)                       [100%] 1 of 1 ✔
[2d/0f0acd] process > make_pop_labels (1)             [100%] 1 of 1 ✔
[4b/bf7347] process > prune_and_check (1)             [100%] 1 of 1 ✔
[d4/f9ae9a] process > run_ADMIXTURE (1)               [100%] 1 of 1 ✔
[d5/b9acee] process > make_fig_csv (1)                [100%] 1 of 1 ✔

Completed at: 22-Dec-2020 17:45:52
Duration    : 32m 19s
CPU hours   : 0.5
Succeeded   : 10
</pre>

## View results

Wherever you specified `resultDir`, you should see a folder with result content after the run. 

{{< fileTree >}}
* yourdata
    * yourdata.bed
    * yourdata.bim
    * yourdata.fam
    * results
        * fig.pdf
        * merged_pruned_seed*.P
        * merged_pruned_seed*.Q
        * proportions.csv
{{< /fileTree >}} 

For a description of these result files, refer to the [output files summary]({{< ref "/output-files-summary" >}}).

