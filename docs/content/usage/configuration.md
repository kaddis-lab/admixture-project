---
title: "Additional configuration"
date: 2020-12-23
weight: 40
---

** This article is incomplete **

This describes most-commonly used additional/alternative configuration of the `nextflow.config` file, which should be modified in cases such as:

- You need/want to give the run more memory. 
- You added another dataset to the project and want to run on that dataset next.
- You want to do multiple runs on the same dataset.

## Memory

The default is `memory = 12000`. This memory configuration is primarily relevant to PLINK processes in the pipeline. If your dataset is much larger than our test dataset, setting a larger number can help resolve pipeline failures due to insufficient memory. 

## Another dataset

The easiest way to run the pipeline with a different target dataset is to swap out the plink files in `user`. However, with multiple datasets it will make more sense to create another directory alongside `user`, e.g. `user2`, with its own data and config containing these modified parameters:
- `userDir = "$projectDir/data/user2"`
- `resultsDir = "$projectDir/data/user2/results"`

When starting a container to work on `user2`, you would use this command:
{{< cmd >}}
docker run -it -v $(pwd):/project/data registry.gitlab.com/kaddis-lab/admixture-project/dev:latest main.nf -c data/user2/nextflow.config
{{</ cmd >}}

## Multiple Runs

It is possible to have the pipeline do multiple ADMIXTURE runs by using multiple unique seeds, e.g. change `seeds = [43]` to `seeds = [43, 47, 53]`. Note that a figure will only be generated for the first seed.









