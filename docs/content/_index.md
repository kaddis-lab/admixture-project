---
date: 2020-12-24
title: "Home"
---

This is project documentation site for our pipeline, which provides ancestry estimation and (soon) ancestry-specific Genetic Risk Score (GRS) for T1D. There are multiple ways to run the pipeline with your data. To get started, head over to [Usage]({{< ref "/usage" >}}). 


### Parent publication(s)

_Pending_

### Other resources
- See [NEWS]({{< ref "/news" >}}) for changes and what's in development.
- For help or requests, submit an [issue](https://gitlab.com/kaddis-lab/admixture-project/-/issues).

