.RECIPEPREFIX = >

VERSION="latest"

base:
> docker build -t registry.gitlab.com/kaddis-lab/admixture-project/base:${VERSION} .

aci: base
> cp main.nf docker-app/aci/ && cd docker-app/aci/ && \
  docker build -t registry.gitlab.com/kaddis-lab/admixture-project/aci:${VERSION} .



