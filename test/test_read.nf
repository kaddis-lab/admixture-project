#!/usr/bin/env nextflow

/*
 * Parameters
 */
params.userDir = "$projectDir/data/user/nPOD"
// params.resultsDir = "$projectDir/data/user/results"
params.is23andMe = false
params.refDir = "$projectDir/data/all_phase3"
params.refsnps = "$projectDir/data/all_phase3/all_phase3_snps"
params.popfile = "$projectDir/data/all_phase3/all_phase3.pop"
params.memory = 12000
params.threads = 12
params.seeds = [43]

/*
 * Creates channel emitting the user input dataset
 */
 if(params.is23andMe) {
   // TODO automatic conversion from .txt
   //

  } else {
  (userdata0, userdata1) = Channel
                .fromFilePairs("${params.userDir}/*.{bed,bim,fam}", size:3, flat:true)
                .ifEmpty { error "No matching user plink files" }
                .into(2)

  N = Channel.fromPath("${params.userDir}/*.fam").countLines()
  }


/*
 * Creates channel emitting the ref dataset
 * ref must have snps from `awk '{print $2}' all_phase3.bim | sort | uniq -d > all_phase3_snps`
*/
refdata = Channel.fromFilePairs("${params.refDir}/*.{bed,bim,fam}", size:3, flat:true)
            .ifEmpty { error "No matching reference plink files" }

popfile = Channel.fromPath("${params.popfile}")
            .ifEmpty { error "No file containing population labels, which is required for supervised analysis" }


/*
 * Print basic
 */
process read_user_files {
   
   echo true 

   input:
   tuple val(id), file(bed), file(bim), file(fam) from userdata1

   shell:
   '''
   echo -------------------------------------------
   echo $(wc -l !{fam}) samples found in user data.
   echo $(wc -l !{bim}) variants found in user data.
   '''
}

process read_ref_files {

   echo true  
   input:
   tuple val(id), file(bed), file(bim), file(fam) from refdata
   
   shell:
   '''
   echo -------------------------------------------
   echo The reference being used is: !{bed.baseName}
   '''
}



