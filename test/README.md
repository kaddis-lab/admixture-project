### Test file structure

This is the test file structure dockerized versions.

```
.
├── test (/project)
│   └── data (/project/data)
│       └── all_phase3
│       └── user
│           ├── nPOD 
│           └── results (output of run results)

```
