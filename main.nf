#!/usr/bin/env nextflow

/*
 * Parameters
 */
params.userDir = "$projectDir/data/user"
params.is23andMe = false
params.refDir = "$projectDir/data/all_phase3"
params.refsnps = "$projectDir/data/all_phase3/all_phase3_snps"
params.popfile = "$projectDir/data/all_phase3/all_phase3.pop"
params.resultsDir = "$projectDir/data/user/results"
params.memory = 12000
params.threads = 12
params.seeds = [43]

/*
 * Creates channel emitting the user input dataset
 */
 if(params.is23andMe) {
   // TODO automatic conversion from .txt
   userdata23 = Channel.fromPath("${params.userDir}/*.txt")
            .ifEmpty { error "No matching 23andMe data file expected as 23andMe-genotyping.txt" }

   userdata1 = Channel.empty()

   N = 1

  } else {
  (userdata0, userdata1) = Channel
                .fromFilePairs("${params.userDir}/*.{bed,bim,fam}", size:3, flat:true)
                .ifEmpty { error "No matching plink user files" }
                .into(2)

  userdata23 = Channel.empty()

  N = Channel.fromPath("${params.userDir}/*.fam").countLines()
  }


/*
 * Creates channel emitting the ref dataset and accessory files
*/
refdata = Channel.fromFilePairs("${params.refDir}/*.{bed,bim,fam}", size:3, flat:true)
            .ifEmpty { error "No matching plink reference files" }

popfile = Channel.fromPath("${params.popfile}")
            .ifEmpty { error "No file containing population labels, which is required for supervised analysis" }

refsnps = Channel.fromPath("${params.refsnps}")
            .ifEmpty { error "No ref snps accessory file" }


/*
 * If given 23andMe data, this will make the plink files and emit it as userdata0 and userdata2
 * Process is mutually exclusive with basic_qc_filters
*/
process convert_23andMe_to_plink {

   input:
   file(txt) from userdata23

   output:
   tuple val(id), file("*.bed"), file("*.bim"), file("*.fam") into userdata0
   file("userdata.{bed,bim,fam}") into userdata2_from23

   script:
   id = txt.baseName
   """
   plink --23file ${txt} --out userdata
   """
}



/*
 * Basic filters for relatedness, etc. see https://www.cog-genomics.org/plink/2.0/distance#king_coefs
 * cutoff of 0.177 removes first-degree and more-related,
 * but if less stringent use 0.354 to remove monozygotic twins and duplicate samples only
 * Note: This does NOT run for 23andMe data, which is 1) already quality-controlled
 * and 2) of size 1, which will fail with KINGCUT filter
*/
process basic_qc_filters {

   input:
   tuple val(id), file(bed), file(bim), file(fam) from userdata1

   output:
   file("userdata.{bed,bim,fam}") into userdata2

   shell:
   '''
   plink2 --memory !{params.memory} --threads !{params.threads} --bfile !{bed.baseName} --king-cutoff 0.177 --allow-extra-chr --snps-only --make-bed --out userdata
   KINGCUT=$(( $(wc -l < *king.cutoff.out.id) - 1 ))
   if [ $KINGCUT > 0 ]
   then
     echo "$KINGCUT related individuals were removed from your dataset"
   fi
   '''
}

/*
 * Get variants in common, outputting a text snp list file
 * Expect 80% of usersnps to be valid rsids; also checks for at least 10K vars in common
*/
process get_intersection_with_check {

    input:
    tuple val(id), file(bed), file(bim), file(fam) from userdata0
    file(refsnps) from refsnps

    output:
    file('intersection') into intersection1, intersection2

    shell:
    '''
    awk '{print $2}' !{bim} | sort | uniq > usersnps
    P=$(echo $(grep -c -E ^rs[0-9]+$ usersnps) '/' $(wc -l < usersnps) | bc -l)
    if (( $(echo "$P > 0.8" | bc -l) ))
    then
      comm -12 !{refsnps} usersnps > intersection
      if (( $(wc -l < intersection) < 10000 ))
      then
        echo "Not enough variants in common"
        exit 1
      fi
    else
      echo "Variants lack standard rs* annotation. Unable to proceed."
      exit 1
    fi
    '''
}


/*
 * Create user input used for merge
*/
process subset_userdata {

    input:
    tuple file(bed), file(bim), file(fam) from userdata2.mix(userdata2_from23)
    file intersection1

    output:
    file("userinput.{bed,bim,fam}") into userinput
    file("userinputsample.bim") into usersamplebim

    script:
    """
    plink --memory ${params.memory} --threads ${params.threads} --bfile ${bed.baseName} --extract $intersection1 --allow-extra-chr --make-bed --out userinput
    head -n 100 userinput.bim > userinputsample.bim
    """
}


/*
 * Create ref input for merge
*/
process subset_refdata {

    input:
    tuple val(id), file(bed), file(bim), file(fam) from refdata
    file intersection2

    output:
    file("refinput.{bed,bim,fam}") into refinput
    file("refinputsample.bim") into refsamplebim

    script:
    """
    plink --memory ${params.memory} --threads ${params.threads} --bfile ${bed.baseName} --extract $intersection2 --allow-extra-chr --make-bed --out refinput
    head -n 100 refinput.bim > refinputsample.bim
    """
}

/*
 * Use the coords of snps in common to compare build AFTER intersection but BEFORE merge
 * Though this seems simple enough to be included in the merge block,
 * this process could eventually evolve to a more extensive one that not only checks build but remaps build
*/
process check_build {

   input:
   file usersamplebim
   file refsamplebim

   output:
   stdout build_check

   shell:
   '''
   test $(awk 'FNR==NR{a[$1,$4];next} (($1,$4) in a)' !{usersamplebim} !{refsamplebim} | wc -l ) -eq 100 && echo true || echo false
   '''
}


/*
 * Merge, also determine number of individuals in userinput
 * Use 'finish' error strategy because want to continue even when PLINK throws a merge error
*/
process merge {

    errorStrategy 'finish '

    input:
    tuple file(refbed), file(refbim), file(reffam) from refinput
    tuple file(userbed), file(userbim), file(userfam) from userinput
    val build_check_result from build_check

    output:
    file("merged.{bed,bim,fam}") into merged


    script:
    if(build_check_result)
      """
      if plink --memory ${params.memory} --threads ${params.threads} --bfile ${refbed.baseName} --bmerge $userbed $userbim $userfam --allow-extra-chr --make-bed --out merge_trial
      then
        mv merge_trial.fam merged.fam
        mv merge_trial.bed merged.bed
        mv merge_trial.bim merged.bim
      else
        plink --bfile ${refbed.baseName} --exclude merge_trial-merge.missnp --make-bed --out ref
        plink --bfile ${userbed.baseName} --exclude merge_trial-merge.missnp --make-bed --out user
        plink --bfile ref --bmerge user --allow-no-sex --allow-extra-chr --make-bed --out merged
      fi
      """
    else
      error "User data does not appear to be the required genomic build (b37). Unable to proceed."

}


/*
 * Generate input .pop file and K parameter given popfile and userdata
 * K is automatically determined as number of unique labels) in *.pop since we're doing supervised
*/
process make_pop_labels {

   input:
   file(popfile) from popfile
   val N

   output:
   tuple file("merged_pruned.pop"), env(K) into pop_input

   script:
   """
   K=\$(cat $popfile | sort | uniq -c | wc -l)
   cp $popfile merged_pruned.pop
   printf '.\n%0.s' {1..$N} >> merged_pruned.pop
   """
}

/*
 * Prune filtering
*/
process prune_and_check {

    input:
    tuple file(bed), file(bim), file(fam) from merged

    output:
    file("merged_pruned.{bed,bim,fam}") into merged_pruned

    script:
    """
    plink --memory ${params.memory} --threads ${params.threads} --bfile ${bed.baseName} --indep-pairwise 50 10 0.1
    plink --memory ${params.memory} --threads ${params.threads} --bfile ${bed.baseName} --extract plink.prune.in --make-bed --out merged_pruned
    if (( \$(wc -l < merged_pruned.bim) < 10000 ))
    then
      echo 'Fewer than 10K variants after pruning. Unable to proceed.'
      exit 1
    fi
    """
}


/*
 * Runs supervised ADMIXTURE and will emit as many results as the unique seeds set
*/
process run_ADMIXTURE {

    publishDir "${params.resultsDir}", mode: "copyNoFollow", overwrite: true

    input:
    tuple file(bed), file(bim), file(fam) from merged_pruned
    tuple file(pops), env(K) from pop_input
    each seed from params.seeds

    output:
    file("*.{P,Q}") into admixture_results
    file pops into pop_for_plot
    file fam into fam_for_plot

    script:
    """
    admixture --supervised -j${params.threads} -s ${seed} $bed \$K > seed${seed}.log
    mv *.5.Q ${bed.baseName}_seed${seed}.Q
    mv *.5.P ${bed.baseName}_seed${seed}.P
    """
}

/*
 * Make R plot and subsetted csv of proportions for results
 * Note that a different R script is used for 23andMe data
*/
process make_fig_csv {

  publishDir "${params.resultsDir}", mode: "copyNoFollow", overwrite: true

  input:
  tuple file(P), file(Q) from admixture_results
  file(pop) from pop_for_plot
  file(fam) from fam_for_plot
  val N

  output:
  file("figure.png") into result_png
  file("figure.pdf") into result_pdf
  file("proportions.csv") into result_csv

  script:
  """
  Rscript /project/plot.R $Q $pop $fam $N
  """
}
