## General notes

This directory hosts build assets for the cloud version of the pipeline image, meant for our cloud application. (Most users should look at the scripts in the main directory if they are interested in the actual workflow.) 

**The cloud version differs from base by adding to the image nextflow and custom web utilities to pass pipeline run-time metadata to our database/app.**

## Internal development notes

If testing locally, set up networking with `-p` mapping as below. 
`docker run -it -p 80:80 --env-file ./env.list -v $(pwd):/project/data registry.gitlab.com/kaddis-lab/admixture-project/aci data/main.nf -C data/nextflow.config -profile test`

`env.list` should contain:

- `USER_UUID=the-user-uuid`
- `PROJECT_UUID=the-project-uuid`
- `API_KEY=auth-key-for-our-endpoint`

Or you can pass in the individual `-env` variables; see https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file. 

