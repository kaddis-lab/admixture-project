# customlog.R
logDigest <- function(status, user, project) {
  customlog <- list(user = user, project = project, event_uuid = uuid::UUIDgenerate(), 
                    event = "", runName = "", runId = "", utcTime = "",
                    process = "", task_id = 0, task_status = "", success = FALSE, errorReport = "")
  customlog$event <- status$event
  customlog$runName <- status$runName
  customlog$runId <- status$runId
  customlog$utcTime <- status$utcTime
  if(customlog$event %in% c("process_submitted", "process_started", "process_completed")) {
    customlog$process <- status$trace$process
    customlog$task_id <- status$trace$task_id
    customlog$status <- status$trace$status
  }
  
  if(customlog$event == "error") {
    # error has neither trace or metadata
  }
  
  if(customlog$event %in% c("started", "completed")) {
    customlog$success <- status$metadata$workflow$success
    if(!customlog$success) customlog$errorReport <- status$metadata$workflow$errorReport
    
  }
  
  jsonlite::minify(jsonlite::toJSON(customlog, auto_unbox = T))
}

#* Meant to receive http POST data from nextflow and forward to a custom endpoint 
#* with additional fields and authorization headers.
#* @param req The request.
#* @post /log
function(req, runName) {
  endpoint <- Sys.getenv("DART_ENDPOINT")
  if(endpoint != "") {
    status <- req$postBody
    user <- Sys.getenv("USER_UUID")
    project <- Sys.getenv("PROJECT_UUID")
    if(user == "") user <- "anonymous_user"
    if(project == "") project <- "temp_project"
    apikey <- Sys.getenv("API_KEY")
    # TO DO
  }
}

#* Meant to receive http POST data from nextflow and forward to a custom endpoint 
#* with additional fields and authorization headers.
#* @param req The request.
#* @post /log_digest
function(req, runName) {
  user <- Sys.getenv("USER_UUID")
  project <- Sys.getenv("PROJECT_UUID")
  if(user == "") user <- "anonymous_user"
  if(project == "") project <- "temp_project"
  apikey <- Sys.getenv("API_KEY")
  endpoint <- Sys.getenv("DART_ENDPOINT") # "https://dev-kdldart.azurewebsites.net/api/NextFlow/JobComplete"
  logdir <- paste0("data/userdata/", user, "/", project, "/log/")
  if(!dir.exists(logdir)) { 
    dir.create(logdir, recursive = T)
  }
  if(endpoint != "") {
    status <- jsonlite::fromJSON(req$postBody)
    if(status$event == "completed") { # For right now, only send final event
      # Send digest to endpoint
      customlog <- logDigest(status, user, project)
      posting <- httr::POST(url = endpoint, httr::content_type_json(), body = customlog, encode = "raw")
      logfile <- paste0(logdir, "completed_", runName, ".txt")
      logline <- paste("endpoint", endpoint, "returned status code", posting$status_code, "at", date())
      cat(logline, customlog, file = logfile)
      posting$status_code
    }
  } 
}

#* Meant to receive http POST data from nextflow and forward to a rtdb endpoint 
#* with additional fields and authorization headers.
#* @param req The request.
#* @post /log_rtdb
function(req, runName) {
  apikey <- Sys.getenv("API_KEY")
  user <- Sys.getenv("USER_UUID")
  project <- Sys.getenv("PROJECT_UUID")
  if(user == "") user <- "anonymous_user"
  if(project == "") project <- "temp_project"
  url <- paste0("https://nextflowdash-default-rtdb.firebaseio.com/logs/", user, "/", project, "/", runName, ".json?auth=", apikey)
  # event_id <- uuid::UUIDgenerate() # rtdb generates an id on its end
  customlog <- jsonlite::minify(req$postBody)
  posting <- httr::POST(url = url, body = customlog, encode = "raw")
  posting$status_code
}


#* Meant to receive http POST data from nextflow and forward to a rtdb endpoint 
#* SIMPLIFIED metadata with authorization headers.
#* @param req The request.
#* @post /log_rtdb_digest
function(req, runName) {
  apikey <- Sys.getenv("API_KEY")
  user <- Sys.getenv("USER_UUID")
  project <- Sys.getenv("PROJECT_UUID")
  if(user == "") user <- "anonymous_user"
  if(project == "") project <- "temp_project"
  url <- paste0("https://nextflowdash-default-rtdb.firebaseio.com/logs/", user, "/", project, "/", runName, ".json?auth=", apikey)
  status <- jsonlite::fromJSON(req$postBody)
  if(status$event == "completed") {
    customlog <- logDigest(status, user, project)
    posting <- httr::POST(url = url, body = customlog, encode = "raw")
    posting$status_code
  }
}

