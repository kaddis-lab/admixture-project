#!/bin/sh

export USER_UUID=$USER_UUID
export PROJECT_UUID=$PROJECT_UUID
export DART_ENDPOINT=$DART_ENDPOINT
export API_KEY=$API_KEY

set -m

Rscript entrypoint.R & nextflow run main.nf -c data/userdata/$USER_UUID/$PROJECT_UUID/nextflow.config
